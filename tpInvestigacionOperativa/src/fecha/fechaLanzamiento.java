package fecha;

public class fechaLanzamiento {
	
	private int anio;
	private int mes;
	private int dia; 
	
	public fechaLanzamiento (int dia, int mes, int anio) {
		this.anio = anio;
		this.mes = mes;
		this.dia = dia; 
	}
	
	public fechaLanzamiento (String resultadoToString) { // resultadoToString = YYYY/MM/DD
		String[] split = resultadoToString.split("/");
		this.anio = Integer.parseInt(split[0]);
		this.mes = Integer.parseInt(split[1]);
		this.dia = Integer.parseInt(split[2]);
	}
	
	public int getAnio() {
		return anio;
	}

	public int getMes() {
		return mes;
	}

	public int getDia() {
		return dia;
	}
	
	public boolean esMasViejoQue(fechaLanzamiento f) {
		if(this.anio < f.getAnio()) {
			return true;
		}else if (this.anio == f.getAnio()) {
			if (this.mes < f.getMes()) {
				return true;
			}else if (this.mes == f.getMes()) {
				if (this.dia < f.getDia()) {
					return true;
				}
				return false;
			} else if (this.mes > f.getMes()) {
				return false;
			}
		} 
		return false;
	}
	
	public boolean mismoAnioOMayor(fechaLanzamiento f) {
		if (this.anio >= f.getAnio()) {
			return true;
		}
		return false; 
	}
	
	public int diferenciaAnios(fechaLanzamiento f) {
		return (this.anio - f.getAnio());
	}
	
	public String toString() {
		return this.anio + "/" + this.mes + "/" + this.dia;
	}

}
