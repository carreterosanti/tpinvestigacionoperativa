package AnalyticAHP;

import java.util.ArrayList;
import java.util.List;

public class AHP_Smartphone extends AHP{

	// INDICES DE CRITERIOS
	static final int DESIGN_MATRIZ = 0;
	static final int SCREEN_MATRIZ = 1;
	static final int CAMERA_MATRIZ = 2;
	static final int SPEED_MATRIZ = 3;
	static final int SOFTWARE_MATRIZ = 4;
	static final int BATTERY_MATRIZ = 5;
	
	private boolean aplicarDiferenciaAnios = false;
	
	
	public AHP_Smartphone() {
		super.addCriterio(DESIGN_MATRIZ);
		super.addCriterio(SCREEN_MATRIZ);
		super.addCriterio(CAMERA_MATRIZ);
		super.addCriterio(SPEED_MATRIZ);
		super.addCriterio(SOFTWARE_MATRIZ);
		super.addCriterio(BATTERY_MATRIZ);		
		super.inicializarMatrizPrioridades();
	}
	
	public void setListaAlternativas(List<Object> listSmartphone) {
		listAlternativas = listSmartphone;
		cantAlternativas = listSmartphone.size();
		super.inicializarMatricesAlternativas();
		super.cargarMatricesAlternativas();
		hashMatrizAlternativas.get(DESIGN_MATRIZ).setId("Matriz Design");
		hashMatrizAlternativas.get(SCREEN_MATRIZ).setId("Matriz Screen");
		hashMatrizAlternativas.get(CAMERA_MATRIZ).setId("Matriz Camera");
		hashMatrizAlternativas.get(SPEED_MATRIZ).setId("Matriz Speed");
		hashMatrizAlternativas.get(SOFTWARE_MATRIZ).setId("Matriz Software");
		hashMatrizAlternativas.get(BATTERY_MATRIZ).setId("Matriz Battery");
	}
	
	public static int getConstanteCriterio(String criterio) {
		switch (criterio) {
		case "Dise�o":
			return DESIGN_MATRIZ;
		case "Pantalla":
			return SCREEN_MATRIZ;
		case "C�mara":
			return CAMERA_MATRIZ;
		case "Rendimiento":
			return SPEED_MATRIZ;
		case "Software":
			return SOFTWARE_MATRIZ;
		case "Bater�a":
			return BATTERY_MATRIZ;
		}
		return -1;
	}
	

	@Override
	protected void setValorMatrizAlternativas(Integer idCriterio, Object object1, Object object2, matrizDinamica matriz, int i, int j) {
		Smartphone smartphone1 = (Smartphone) object1;
		Smartphone smartphone2 = (Smartphone) object2;
		int diferencia;

		/* EJEMPLO DIFERENCIA ANIOS
			(Smartphone1 = 2016	Valor = 8)		(Smartphone2 = 2018 Valor = 9) diferencia = -1  diferenciaAnio = -2 ...	m(j,i)= ((-diferencia)+1)+(-diferenciaAnio)
			(Smartphone1 = 2016	Valor = 9)		(Smartphone2 = 2018 Valor = 8) diferencia = 1  diferenciaAnio = -2 ...	m(i,j)= (diferencia+1)
		*
		*/
		int diferenciaAnio = smartphone1.getFechaLanzamiento().getAnio() - smartphone2.getFechaLanzamiento().getAnio(); 
		switch (idCriterio) {
		case CAMERA_MATRIZ:
			if (aplicarDiferenciaAnios) {
				if (diferenciaAnio < 0) {
					diferencia = smartphone1.getCamera() - (smartphone2.getCamera()+(1)); 
				} else if (diferenciaAnio > 0) {
					diferencia = (smartphone1.getCamera()+1) - smartphone2.getCamera(); 
					
				} else {
					diferencia = smartphone1.getCamera() - smartphone2.getCamera();
				}
			} else {
				diferencia = smartphone1.getCamera() - smartphone2.getCamera();
			}
			if (diferencia < 0) {
				matriz.setEnEspejo(j, i, (-diferencia)+1); // Se pasa el valor a positivo y se le suma uno. 
			} else if (diferencia > 0) {
					matriz.setEnEspejo(i, j, diferencia+1);
				} else {
					matriz.setEnEspejo(i, j, 1);
				}
			break;
		case DESIGN_MATRIZ:
			diferencia = smartphone1.getDesign() - smartphone2.getDesign(); 
			if (diferencia < 0) {
				matriz.setEnEspejo(j, i, (-diferencia)+1); // Se pasa el valor a positivo y se le suma uno. 
			} else if (diferencia > 0) {
					matriz.setEnEspejo(i, j, diferencia+1);
				} else {
					matriz.setEnEspejo(i, j, 1);
				}
			break;
		case SCREEN_MATRIZ:
			diferencia = smartphone1.getScreen() - smartphone2.getScreen(); 
			if (diferencia < 0) {
				matriz.setEnEspejo(j, i, (-diferencia)+1); // Se pasa el valor a positivo y se le suma uno. 
			} else if (diferencia > 0) {
					matriz.setEnEspejo(i, j, diferencia+1);
				} else {
					matriz.setEnEspejo(i, j, 1);
				}
			break;
		case SOFTWARE_MATRIZ:
			diferencia = smartphone1.getSoftware() - smartphone2.getSoftware(); 
			if (diferencia < 0) {
				matriz.setEnEspejo(j, i, (-diferencia)+1); // Se pasa el valor a positivo y se le suma uno. 
			} else if (diferencia > 0) {
					matriz.setEnEspejo(i, j, diferencia+1);
				} else {
					matriz.setEnEspejo(i, j, 1);
				}
			break;
		case SPEED_MATRIZ:
			if (aplicarDiferenciaAnios) {
				if (diferenciaAnio < 0) {
					diferencia = smartphone1.getSpeed() - (smartphone2.getSpeed()+(1)); 
				} else if (diferenciaAnio > 0) {
					diferencia = (smartphone1.getSpeed()+1) - smartphone2.getSpeed(); 
					
				} else {
					diferencia = smartphone1.getSpeed() - smartphone2.getSpeed();
				}
			} else {
				diferencia = smartphone1.getSpeed() - smartphone2.getSpeed();
			}
			if (diferencia < 0) {
				matriz.setEnEspejo(j, i, (-diferencia)+1); // Se pasa el valor a positivo y se le suma uno. 
			} else if (diferencia > 0) {
					matriz.setEnEspejo(i, j, diferencia+1);
				} else {
					matriz.setEnEspejo(i, j, 1);
				}
			break;
		case BATTERY_MATRIZ:
			if (aplicarDiferenciaAnios) {
				if (diferenciaAnio < 0) {
					diferencia = smartphone1.getBattery() - (smartphone2.getBattery()+(1)); 
				} else if (diferenciaAnio > 0) {
					diferencia = (smartphone1.getBattery()+1) - smartphone2.getBattery(); 
					
				} else {
					diferencia = smartphone1.getBattery() - smartphone2.getBattery();
				}
			} else {
				diferencia = smartphone1.getBattery() - smartphone2.getBattery();
			}
			if (diferencia < 0) {
				matriz.setEnEspejo(j, i, (-diferencia)+1); // Se pasa el valor a positivo y se le suma uno. 
			} else if (diferencia > 0) {
					matriz.setEnEspejo(i, j, diferencia+1);
				} else {
					matriz.setEnEspejo(i, j, 1);
				}
			break;
		}	
	}


	
}
