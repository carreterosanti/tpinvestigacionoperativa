package AnalyticAHP;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.swing.plaf.synth.SynthSeparatorUI;

import admArchivos.admArchivo;
import fecha.fechaLanzamiento;
import javafx.concurrent.Task;
import webScrap.JsoupRun;

public class Analityc {

	private admArchivo admArchivos = new admArchivo();
	private HashMap<String, Smartphone> hashSmartphones = new HashMap<>();
	private Smartphone dispositivoElegido = null;
	private AHP_Smartphone ahp_Smartphone; 
	private float valorTotalInversion = -1;
	private float valorInversionMas50Porc = -1; 
	private float valorInverionDoble = -1; 
	private Integer nroS = 1;
	
	private List<Thread> listaThreads;
	private List<List<Smartphone>> listaResultadosThread = new ArrayList<>();
	private ThreadGroup grupoThread = new ThreadGroup("GroupThread");
	

	private AHP_Smartphone ahp50PorcientoMas; 
	private AHP_Smartphone ahp100PorcientoMas; 
	


	public Analityc () {
		ahp_Smartphone = new AHP_Smartphone();
	}
	
	public HashMap<String, Smartphone> getHashSmartphones() {
		return hashSmartphones;
	}
	
	public float getValorInversionMas50Porc() {
		return valorInversionMas50Porc;
	}

	public float getValorInverionDoble() {
		return valorInverionDoble;
	}
	
	public ThreadGroup getGrupoThread() {
		return grupoThread;
	}

	public Smartphone getDispositivoElegido() {
		return dispositivoElegido;
	}	
	
	public AHP_Smartphone getAhp50PorcientoMas() {
		return ahp50PorcientoMas;
	}

	public AHP_Smartphone getAhp100PorcientoMas() {
		return ahp100PorcientoMas;
	}

	public void setDispositivoElegido(Smartphone dispositivoElegido) {
		this.dispositivoElegido = dispositivoElegido;
	}
	
	public void levantarBasedeDatos (List<String> listaDispositivosUI) throws NumberFormatException, IOException{
		BufferedReader br;
		br = admArchivos.levantarBaseDeDatos("Lista Celulares todas las Gamas.txt");
    	String st;
		while ((st = br.readLine()) != null) {
			String[] smp = st.split("/ ");
			// SMP 0=Nombre 1=FechaLanzamiento 2=Gama 3=PrecioUsado 4=PrecioNuevo 5=Dise�o 6=Pantalla 7=Camara 8=Rendimiento 9=Autonomia 10=Software 11=urlGadget		
			Smartphone nuevo = new Smartphone(smp[0].substring(8), new fechaLanzamiento(smp[1].substring(19)), smp[2].substring(6), smp[11].substring(10));
			nuevo.setPuntajes(Integer.parseInt(smp[5].substring(8)), Integer.parseInt(smp[6].substring(10)), Integer.parseInt(smp[7].substring(8)), Integer.parseInt(smp[7].substring(8)), Integer.parseInt(smp[9].substring(11)), Integer.parseInt(smp[10].substring(10)));
			hashSmartphones.put(nuevo.getNombre(), nuevo);
			listaDispositivosUI.add(smp[0].substring(8));
		}
	}

	public AHP_Smartphone getAhp_Smartphone() {
		return ahp_Smartphone;
	}


	public float getValorTotalInversion() {
		return valorTotalInversion;
	}

	public void setValorTotalInversion(float valorTotalInversion) {
		this.valorTotalInversion = valorTotalInversion;
	}
	
	public List<Smartphone> getSubListNSmartphones(int nroThread, int nroTotalProcesadores){
		int nroElementosPorThread = hashSmartphones.size() / nroTotalProcesadores;
		//System.out.println("Nro Thread: " + nroThread);
		List<Smartphone> listaTotal = new ArrayList<>();
		for (String nombreSmartphone: hashSmartphones.keySet())
			listaTotal.add(hashSmartphones.get(nombreSmartphone));
		if (nroThread < nroTotalProcesadores) {
			//System.out.println("Rango de Valores: " + ((nroThread-1)*nroElementosPorThread) + ((nroElementosPorThread*nroThread)-1));
			return listaTotal.subList(((nroThread-1)*nroElementosPorThread), ((nroElementosPorThread*nroThread)-1));
		} 
		else if (nroThread == nroTotalProcesadores) {
			//System.out.println("Rango de Valores: " + ((nroThread-1)*nroElementosPorThread) + (hashSmartphones.size()-1));
			return listaTotal.subList(((nroThread-1)*nroElementosPorThread), (hashSmartphones.size()-1));
		}
		return null;
		
	}
	
	public void realizarAHPConPorcentajePresupuestos(List<Object> listaTop50, List<Object> listaTop100) {
		valorInversionMas50Porc = (float) (valorTotalInversion + (valorTotalInversion * 0.5));
		valorInverionDoble = (valorTotalInversion * 2);
		//DECLARACION DE LOS OBJETOS AHP_SMARTPHONE EL CUAL INCLUYE LA INICIALIZACION DE LOS CRITERIOS CORRESPONDIENTES A SMARTPHONE
		ahp50PorcientoMas = new AHP_Smartphone(); 
		ahp100PorcientoMas = new AHP_Smartphone(); 
		List<Object> listaAlternativas = new ArrayList<>(); 
		listaAlternativas.addAll(hashSmartphones.values()); 
		// FILTRO POR FECHA DE INICIO SEGUN EL SMARTPHONE INDICADO POR EL USUARIO. 
		listaAlternativas = Smartphone.filtroPorFecha(listaAlternativas, dispositivoElegido.getFechaLanzamiento());
		// FILTRAR POR PRECIO, SIENDO EL DOBLE DE LA INVERSION INDICADA POR EL USUARIO. 
		listaAlternativas = Smartphone.filtroPrecioMenor(listaAlternativas, valorInverionDoble);
		ahp100PorcientoMas.setListaAlternativas(listaAlternativas);
		// FILTRAR POR PRECIO, SIENDO EL DOBLE DE LA INVERSION INDICADA POR EL USUARIO. 
		listaAlternativas = Smartphone.filtroPrecioMenor(listaAlternativas, valorInversionMas50Porc);
		ahp50PorcientoMas.setListaAlternativas(listaAlternativas);
		listaTop100.addAll(ahp100PorcientoMas.generarAnalisisAHP());
		listaTop50.addAll(ahp50PorcientoMas.generarAnalisisAHP());
		
	}
	
	public boolean busquedaNewPriceActiva() {
		return false;
	}
	
	public int totalResultadosThread() {
		int total = 0; 
		for (int i = 0; i < listaResultadosThread.size(); i++) {
			for (Smartphone s: listaResultadosThread.get(i)) {
				total++; 
			}
		}
		return total;
	}
	
	public void iniciarWebScrapNewPrice() {

		if (!hashSmartphones.isEmpty()) {
	    //	System.out.println("Numero de proesadores: " + Runtime.getRuntime().availableProcessors());
	    	int nroProcesadoresDisponibles = Runtime.getRuntime().availableProcessors();
	    	System.out.println("Numero de threads: " + nroProcesadoresDisponibles);
	    	for (int i = 1; i <= nroProcesadoresDisponibles; i++) {
	    		listaResultadosThread.add(new ArrayList<>(getSubListNSmartphones(i, nroProcesadoresDisponibles)));
	    		Task getNewPriceWorker = createNewPriceWorker(i-1);
	    		Thread th = new Thread(getNewPriceWorker);
	    		th = new Thread(grupoThread, getNewPriceWorker);
	    		th.setName(i + "");
	    		th.start();
	    	}
		}
		
	}
			
	
	public Task createNewPriceWorker(int name) {
    	return new Task() {

			@Override
			protected Object call() throws Exception {
				JsoupRun js = new JsoupRun();
				try {
					for (Smartphone smartphone: listaResultadosThread.get(name)) {	
						js.obtenerPrecioNuevo(smartphone);	
						System.out.println(smartphone.getNombre() + smartphone.getPrecioNuevo());
						
					}
				} catch (IOException e) {
					e.printStackTrace();
					//System.out.println("ENTRO POR ACA");
				}
				return null;
			}
    	};
	}
	
	
}
