package AnalyticAHP;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javafx.scene.image.Image;

public abstract class AHP {

	private static final int consNumeroSubdivisionMatrices = 10;
	
	protected int cantAlternativas;
	private matrizDinamica matrizPrioridadCriterios;
	protected List<Integer> listCriterios = new ArrayList<>();
	protected HashMap<Integer,matrizDinamica> hashMatrizAlternativas = new HashMap<>();
	protected List<Object> listAlternativas;
	
	
	
	// PASAS CANTIDAD DE ALTERNATIVAS PARA CREAR LAS MATRICES
	public AHP () {
		
	}
	
	public Object getAlternativa(int i) {
		return listAlternativas.get(i);
	}
	
	public void addCriterio (Integer criterio) {
		listCriterios.add(criterio);
	}
	
	public void cargarMatricesAlternativas() {
		if (!listCriterios.isEmpty()) {
			for (Integer idCriterio: listCriterios) 
				for (int i=0; i < listAlternativas.size(); i++) 
					for (int j = i+1; j < listAlternativas.size(); j++) {
						setValorMatrizAlternativas(idCriterio, listAlternativas.get(i), listAlternativas.get(j), hashMatrizAlternativas.get(idCriterio), i , j);
					}
		}
	}
	
	public abstract void setListaAlternativas(List<Object> listSmartphone);
	
	public matrizDinamica getMatrizPrioridadCriterios() {
		return matrizPrioridadCriterios;
	}
	
	public List<Object> getListaAlternativas(){
		return listAlternativas; 
	}
	
	protected abstract void setValorMatrizAlternativas(Integer idCriterio, Object object1, Object object2, matrizDinamica matriz, int i, int j);
	
	public void setValorMatrizCriterios (int x, int y, float valor) {
		matrizPrioridadCriterios.setEnEspejo(x, y, valor);
	}
	
	public void inicializarMatrizPrioridades () {
		matrizPrioridadCriterios = new matrizDinamica(listCriterios.size(), listCriterios.size(), 1);
	}
	
	public void inicializarMatricesAlternativas () {
		for (Integer idCriterio: listCriterios) {
			hashMatrizAlternativas.put(idCriterio, new matrizDinamica(cantAlternativas, cantAlternativas, 1));
		}
	}
	
	
	public static double calcularCocienteDeConsistenciaCriteriosPrueba(matrizDinamica matrizInput) {
		matrizDinamica matrizNormalizada = matrizInput.obtenerMatrizNormalizada(); 
		System.out.println("Matriz normalizada: ");
		List<Float> vectorSumatoriaPorFilaMatrizInputXMatrizNormalizada = matrizInput.getVectorMultiplicandoPorFilaDeMatriz(matrizNormalizada);
		System.out.println("Vector MxMN: " + vectorSumatoriaPorFilaMatrizInputXMatrizNormalizada);
		System.out.println("");
		List<Float> vectorSumatoriaPorFila = matrizNormalizada.getVectorSumaPorFila(); 
		System.out.println("Vector Sumatoria por fila por el cual dividir: " + vectorSumatoriaPorFila);
		System.out.println("");

		List<Float> vectorAux = new ArrayList<>(); 
		for (int i=0; i < vectorSumatoriaPorFilaMatrizInputXMatrizNormalizada.size(); i++) {
			vectorAux.add(vectorSumatoriaPorFilaMatrizInputXMatrizNormalizada.get(i)/vectorSumatoriaPorFila.get(i));
		}
		System.out.println("Vector Aux: " + vectorAux);
		
		float promedioVectorAux = 0; 
		for (Float f: vectorAux) {
			promedioVectorAux += f;
		}
		promedioVectorAux = promedioVectorAux / vectorAux.size();
		System.out.println("Promedio Aux: " + promedioVectorAux);
		
		
		float cI = (promedioVectorAux - vectorAux.size()) / (vectorAux.size()-1); 
		
		System.out.println("CI: " + cI);
		
		int n = matrizInput.getDimension();
		
		System.out.println("n: " + n);
		
		double rI = (((2.7699*n) - 4.3513) - n) / (n - 1);  // rI aproximado mediante el metodo propuesto por Alonso y Lamata en el paper: CONSISTENCY IN THE ANALYTIC HIERARCHY PROCESS: A NEW APPROACH
		
		System.out.println("RI: " + rI);
		
		return (cI / rI); 
	}
	
	public double calcularCocienteDeConsistencia(matrizDinamica matrizInput) {
		matrizDinamica matrizNormalizada = matrizInput.obtenerMatrizNormalizada(); 

		List<Float> vectorSumatoriaPorFilaMatrizInputXMatrizNormalizada = matrizInput.getVectorMultiplicandoPorFilaDeMatriz(matrizNormalizada);
		
		List<Float> vectorSumatoriaPorFila = matrizNormalizada.getVectorSumaPorFila(); 
		
		List<Float> vectorAux = new ArrayList<>(); 
		for (int i=0; i < vectorSumatoriaPorFilaMatrizInputXMatrizNormalizada.size(); i++) {
			vectorAux.add(vectorSumatoriaPorFilaMatrizInputXMatrizNormalizada.get(i)/vectorSumatoriaPorFila.get(i));
		}
		float promedioVectorAux = 0; 
		for (Float f: vectorAux) {
			promedioVectorAux += f;
		}
		promedioVectorAux = promedioVectorAux / vectorAux.size();
		
		float cI = (promedioVectorAux - vectorAux.size()) / (vectorAux.size()-1); 
		int n = matrizInput.getDimension();
		
		double rI = (((2.7699*n) - 4.3513) - n) / (n - 1);  // rI aproximado mediante el metodo propuesto por Alonso y Lamata en el paper: CONSISTENCY IN THE ANALYTIC HIERARCHY PROCESS: A NEW APPROACH
		
		return (cI / rI); 
	}
	
	
	public void imprimirCocienteDeConsistenciaMatricesInvolucradas() {
		System.out.println("Matriz Prioridad Criterios: " + "CR: " + calcularCocienteDeConsistenciaCriteriosPrueba(matrizPrioridadCriterios));
		System.out.println("Matrices Alternativas: ");
		List<matrizDinamica> matrices = new ArrayList<>(); 
		matrices.addAll(hashMatrizAlternativas.values());
		for (int i = matrices.size() -1; i >= 0; i--) {
			System.out.println("Criterio " + matrices.get(i).getId() + " Size: " + matrices.get(i).getDimension() + " CR: " + calcularCocienteDeConsistenciaCriteriosPrueba(matrices.get(i)));
		}
	}
	
	public boolean isMatrizPrioridadCrioteriosConsistente() {
		double valorCR = calcularCocienteDeConsistenciaCriteriosPrueba(matrizPrioridadCriterios);
		if (valorCR > 0.1)
			return false;
		return true;
	}
	
	
	public List<Float> determinarCandidatos (List<Float> criteriosV, List<List<Float>> listVectoresAlternativas){
		List<Float> scoresGlobales = new ArrayList<>();
		for (int i=0; i < cantAlternativas; i++){
			float total=0;
			for (Integer idCriterio: listCriterios) {
				total+= criteriosV.get(idCriterio)*listVectoresAlternativas.get(idCriterio).get(i);
			}
			scoresGlobales.add(total);
		}
		return scoresGlobales;
	}

	
	public List<matrizDinamica> getSubMatricesNxN(List<Object> listaAlternativas){
		List<matrizDinamica> listaResultado = new ArrayList<>(); 	
		matrizDinamica matrizPrueba = new matrizDinamica(consNumeroSubdivisionMatrices, consNumeroSubdivisionMatrices);
		return listaResultado;
	}
	
	
	
	public List<Object> generarAnalisisAHP(){
		
		// LISTA RESULTADOS
		List<Object> resultados = new ArrayList<>(); // Lista de resultados. Posicion/Top   0/1   1/2   2/3
		
		// MATRIZ DE COMPARACION POR PARES - CRITERIOS
		matrizDinamica normaCriterios = matrizPrioridadCriterios.obtenerMatrizNormalizada();
		
		// MATRIZ DE COMPARACION POR PARES - ALTERNATIVAS
		List<matrizDinamica> matrizNormalizadas = new ArrayList<>();
		for (Integer idCriterio: listCriterios) {
			matrizNormalizadas.add(hashMatrizAlternativas.get(idCriterio).obtenerMatrizNormalizada());
		}
		
		// CREO VECTOR PARA CADA MATRIZ NORMALIZADA
		// VECTOR DE CRITERIOS
		List<Float> vectorCriterios = normaCriterios.getVectorSumaPorFila();
		
		// VECTORES DE ALTERNATIVAS
		List<List<Float>> listVectoresAlternativas = new ArrayList<>();
		for (Integer idCriterio: listCriterios) {
			listVectoresAlternativas.add(matrizNormalizadas.get(idCriterio).getVectorSumaPorFila());
		}
		
		// DETERMINO LOS CANDIDATOS
		List<Float> vectorResultado = determinarCandidatos(vectorCriterios, listVectoresAlternativas);
	//	for (float f:vectorResultado) {
	//		System.out.println(f);
	//	}
		
		if (vectorResultado.size() >= 3) {
			// OBTENGO EL MAXIMO DE LA LISTA ES EL CANDIDATO GANADOR
			float maximo = Collections.max(vectorResultado);
			int position = vectorResultado.indexOf(maximo);
			
			resultados.add(listAlternativas.get(position)); //TOP 1
			resultados.add(maximo);
			vectorResultado.remove(position);
			
			
			maximo = Collections.max(vectorResultado);
			position = vectorResultado.indexOf(maximo);
			
			resultados.add(listAlternativas.get(position)); //TOP 2
			resultados.add(maximo);
			vectorResultado.remove(position);
			
			maximo = Collections.max(vectorResultado);
			position = vectorResultado.indexOf(maximo);
			
			resultados.add(listAlternativas.get(position)); // TOP 3
			resultados.add(maximo);
			vectorResultado.remove(position);
		}
		

		
		
		
		return resultados;
	}

	
	public static void main(String[] args) {
		
		
		matrizDinamica matrizTestCC = new matrizDinamica(3, 3);
		matrizTestCC.setDiagonal(1);
		matrizTestCC.setEnEspejo(0, 1, 1);
		matrizTestCC.setEnEspejo(0, 2, 1);
		matrizTestCC.setEnEspejo(1, 2, 1);
		
		matrizTestCC.imprimirMatriz();
		System.out.println("Valor de CC: " + calcularCocienteDeConsistenciaCriteriosPrueba(matrizTestCC));		
		/*
		List<Object> lista = new ArrayList<>();
		Smartphone uno = new Smartphone("uno", null, null, null);
		uno.setPuntajes(8, 9, 7, 6, 8, 8);
		lista.add(uno);
		Smartphone dos = new Smartphone("dos", null, null, null);
		dos.setPuntajes(5, 6, 7, 7, 8, 9);
		lista.add(dos);
		Smartphone tres = new Smartphone("tres", null, null, null);
		tres.setPuntajes(9, 9, 5, 5, 7, 7);
		lista.add(tres);
		Smartphone cuatro = new Smartphone("cuatro", null, null, null);
		cuatro.setPuntajes(7, 8, 8, 6, 7, 9);
		lista.add(cuatro);
		
		
		AHP analisis = new AHP_Smartphone();
		
		analisis.setListaAlternativas(lista);
		
		analisis.setValorMatrizCriterios(0, 1, 2);
		analisis.setValorMatrizCriterios(0, 2, 8);
		analisis.setValorMatrizCriterios(0, 3, 8);
		analisis.setValorMatrizCriterios(4, 0, 5);
		analisis.setValorMatrizCriterios(5, 0, 5);
		analisis.setValorMatrizCriterios(1, 2, 3);
		analisis.setValorMatrizCriterios(3, 1, 5);
		analisis.setValorMatrizCriterios(4, 1, 6);
		analisis.setValorMatrizCriterios(1, 5, 2);
		analisis.setValorMatrizCriterios(2, 3, 1);
		analisis.setValorMatrizCriterios(2, 4, 8);
		analisis.setValorMatrizCriterios(2, 5, 3);
		analisis.setValorMatrizCriterios(4, 3, 3);
		analisis.setValorMatrizCriterios(3, 5, 3);
		analisis.setValorMatrizCriterios(4, 5, 9);
		*/
		//int res = analisis.generarAnalisisAHP();
		
		//System.out.println("Alternativa: " + res);
		
	}
}
