package AnalyticAHP;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.text.StyleContext.SmallAttributeSet;

import fecha.fechaLanzamiento;

public class Smartphone {
	
	private String nombre = null; 
	private fechaLanzamiento fechaLanzamiento = null; 
	private String gama = null; // Valores validos: Alta, Media y Baja
	private float precioUsado = -1; 
	private float precioNuevo = -1; 
	private String urlGadget = null; 
	
	private int design = -1; 
	private int screen = -1;
	private int camera = -1; 
	private int speed = -1; 
	private int battery = -1; 
	private int software = -1; 
	
	public Smartphone (String nombre, fechaLanzamiento fechaLanzamiento, String gama, String urlGadget) {
		this.nombre = nombre; 
		this.fechaLanzamiento = fechaLanzamiento; 
		this.gama = gama; 
		this.urlGadget = urlGadget;
	}
	
	public void setFechaLanzamiento(fechaLanzamiento date) {
		this.fechaLanzamiento = date;
	}
	
	public void setPuntajes(int d, int p, int c, int r, int a, int s) {
		design = d; 
		screen = p; 
		camera = c; 
		speed = r; 
		battery = a; 	
		software = s; 
	}
	
	public void setPrecioUsado(float precioUsado) {
		this.precioUsado = precioUsado;
	}

	public void setPrecioNuevo(float precioNuevo) {
		this.precioNuevo = precioNuevo;
	}

	public String getNombre() {
		return nombre;
	}

	public fechaLanzamiento getFechaLanzamiento() {
		return fechaLanzamiento;
	}

	public String getGama() {
		return gama;
	}
	
	public String getUrlGadget() {
		return urlGadget;
	}

	public float getPrecioUsado() {
		return precioUsado;
	}

	public float getPrecioNuevo() {
		return precioNuevo;
	}
	
	public int getDesign() {
		return design;
	}

	public int getScreen() {
		return screen;
	}

	public int getCamera() {
		return camera;
	}

	public int getSpeed() {
		return speed;
	}

	public int getBattery() {
		return battery;
	}

	public int getSoftware() {
		return software;
	}

	// FILTRA EL PRECIO DE LA HASHMAP DE ANALITYCS Y DEVUELVE FILTRADO EN LISTA DE OBJETOS
	@SuppressWarnings("rawtypes")
	public static List<Object> filtroPrecioMenor (List<Object> listSmartphones, float precio) {
		List<Object> listResul = new ArrayList<>();
		for (Object phone: listSmartphones) {
			if (((Smartphone) phone).getPrecioNuevo() <= precio && ((Smartphone) phone).getPrecioNuevo() >= 0)
				listResul.add(phone);
		}
		return listResul;
	}
	
	// FILTRAR UNA HASH DE SMARTPHONES SEGUN UNA FECHA DADA 
	public static List<Object> filtroPorFecha (List<Object> listSmartphones, fechaLanzamiento fechaInicio) {
		List<Object> listResult = new ArrayList<>();
		System.out.println("Tama�o lista entrada: " + listSmartphones.size());
		for (Object phone: listSmartphones) {
			if (((Smartphone) phone).getFechaLanzamiento().mismoAnioOMayor(fechaInicio))
				listResult.add(phone);
		}
		System.out.println("Tama�o retorno: " + listResult.size());
		return listResult; 
	}
	
	public String toString() {
		return ("Nombre: " + nombre + "/ Fecha Lanzamiento: " + fechaLanzamiento.toString() + "/ Gama: " + gama + "/ Precio usado: " + precioUsado + "/ Precio nuevo: " + precioNuevo + "/ Diseno: " + design + "/ Pantalla: " + screen + "/ Camara: " + camera + "/ Rendimiento: " + speed + "/ Autonomia: " + battery + "/ Software: " + software + "/ urlGadget: " + urlGadget);
	}	
	
}
