package AnalyticAHP;

import java.util.ArrayList;
import java.util.List;

public class matrizDinamica {
	
	private String id = null; 
	private List<List<Float>> matriz = new ArrayList<>();
	private int fil;
	private int col;
	
	public matrizDinamica(int fil, int col) {
		
		this.fil = fil;
		this.col = col;
		
		//Inicializar matriz 
		for (int i=0; i < fil; i++) {
			matriz.add(new ArrayList<Float>());
			for (int j=0; j < col; j++) {
				matriz.get(i).add((float) 0);
			}
		}
	}
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}
	
	
	public int getDimension() {
		if (fil == col) {
			return fil;
		}
		return -1;
	}
	
	public matrizDinamica(int fil, int col, float valorDiagonal) {

		this.fil = fil;
		this.col = col;

		//Inicializar matriz 
		for (int i=0; i < fil; i++) {
			matriz.add(new ArrayList<Float>());
			for (int j=0; j < col; j++) {
				matriz.get(i).add((float) 0);
			}
		}
		setDiagonal(valorDiagonal);
	}
	
	public void setDiagonal (float valorDiagonal){
		for (int i=0; i < fil; i++){
			setNumber(i, i, valorDiagonal);
		}
	}

	public void setEnEspejo (int x, int y, float valor) {
		setNumber(x, y, valor);
		setNumber(y, x, (float) 1/valor);
	}
	
	public int getFilas() {
		return fil;
	}

	public int getColumnas() {
		return col;
	}

	public void setNumber(int x, int y, float valor ) {
		matriz.get(x).set(y,valor);
	}
	
	public Float getNumber(int x, int y) {
		return matriz.get(x).get(y);
	}
	
	public float getSumaColumna (int col){
		float total = 0;
		for (int i=0; i < fil; i++){
			total+= matriz.get(i).get(col);
		}
		return total;
	}
	
	public float getSumaFila (int fil){
		float total = 0;
		for (int i=0; i < col; i++){
			total+= matriz.get(fil).get(i);
		}
		return total;
	}
	
	public matrizDinamica obtenerMatrizNormalizada (){
		matrizDinamica matrizNor = new matrizDinamica(fil, col);
		for (int i=0; i < fil; i++) {
			for (int j=0; j < col; j++) {
				matrizNor.setNumber(i, j, (getNumber(i, j)/getSumaColumna(j)));
			}
		}
		return matrizNor;
	}
	
	public List<Float> getVectorFila(int numeroFila){
		List<Float> resultado = new ArrayList<>(); 
		if ((numeroFila < fil) && (numeroFila >= 0)) {
			for (int j=0; j < col; j++) {
				resultado.add(matriz.get(numeroFila).get(j));
			}
		return resultado;
		}
		return null; 
	}
	
	public List<Float> getVectorMultiplicandoPorFilaDeMatriz(matrizDinamica matrizInput){
		List<Float> listaResultado = new ArrayList<>(); 
		float aux = 0; 
		for (int i=0; i < fil; i++) {
			List<Float> vectorFilaMatrizInput = matrizInput.getVectorSumaPorFila(); // OBTENGO EL VECTOR FILA CORRESPONDIENTE A LA MATRIZ INPUT
		//	System.out.println("Vector Matriz Normalizada: " + vectorFilaMatrizInput);
			for (int j=0; j < col; j++) { 
				aux = aux + (this.getNumber(i, j) * vectorFilaMatrizInput.get(j)); // RECORRO LAS COLUMNAS DE LA FILA i MULTIPLICANDO EL VALOR POR EN LA FILA i DE LA MATRIZ INPUT
			}
			listaResultado.add(aux); // ALMACENO EL VALOR DE LA MULTIPLICACION DE LA FILAxFILA
			aux = 0; 
		}
		return listaResultado; 
	}
	
	public List<Float> getVectorSumaPorFila(){
		List<Float> vector = new ArrayList<>();
		for (int i=0; i < fil; i++){
			vector.add(getSumaFila(i)/fil);
		}
		return vector;
	}
	
	public List<Float> getVectorSumaPorColumna(){
		List<Float> vector = new ArrayList<>();
		for (int j=0; j < col; j++){
			vector.add(getSumaColumna(j)/col);
		}
		return vector;
	}
	
	public void imprimirMatriz() {
		
		for (int i=0; i < fil; i++) {
			for (int j=0; j < col; j++) {
				System.out.print(matriz.get(i).get(j) + " ");
			}
			System.out.println();
		}
		
	}
	

}
