package webScrap;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectOutputStream.PutField;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.spi.CalendarDataProvider;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.sun.prism.Image;

import AnalyticAHP.Smartphone;
import admArchivos.admArchivo;
import fecha.fechaLanzamiento;
import javafx.util.converter.LocalDateStringConverter;

public class JsoupRun {
	
	private static int numeroPrecioNuevo = 0; 
	private static int numeroPrecioUsado = 0; 
	
	
	
	public static fechaLanzamiento dateParser(String rawDate) { //El formato de rawDate es: NombreMesEnIngles NumeroA�o
		if (!rawDate.isEmpty()) {
		//	System.out.println(rawDate);
			int year = Integer.parseInt(rawDate.substring(rawDate.length()-4)); //Ultimos 4 char de rawDate
		//	System.out.println(year + "");
			String month = rawDate.substring(0, rawDate.length()-5);
			String stringFecha = null;
			switch (month) {
			case "January":
				stringFecha = year + "/0" + 1 +  "/01"; 
				break;
			case "February":
				stringFecha = year + "/0" + 2 +  "/01"; 
				break;
			case "March":
				stringFecha = year + "/0" + 3 +  "/01"; 
				break;
			case "April":
				stringFecha = year + "/0" + 4 +  "/01"; 
				break;
			case "May":
				stringFecha = year + "/0" + 5 +  "/01"; 
				break;
			case "June":
				stringFecha = year + "/0" + 6 +  "/01"; 
				break;
			case "July":
				stringFecha = year + "/0" + 7 +  "/01"; 
				break;
			case "August":
				stringFecha = year + "/0" + 8 +  "/01"; 
				break;
			case "September":
				stringFecha = year + "/0" + 9 +  "/01"; 
				break;
			case "October":
				stringFecha = year + "/" + 10 +  "/01"; 
				break;
			case "November":
				stringFecha = year + "/" + 11 +  "/01"; 
				break;
			case "December":
				stringFecha = year + "/" + 12 +  "/01"; 
				break;
			}
			return new fechaLanzamiento(stringFecha);
		}
		return null;
	}
	
	public String getImageURL(Smartphone smp) throws IOException {
		Document d = Jsoup.connect(smp.getUrlGadget()).timeout(60000).get();
		Elements image = d.select("ul.swiper-wrapper");
		if (image.select("img").attr("src") != "") {
			return image.select("img").attr("src");
		}else 
		{
			return d.select("div.product_detail_wrp").select("img").attr("src");
		}
	}
	
	public static void generarBaseDeDatos(BufferedReader listaNombresCelulares, admArchivo lA) throws IOException, ParseException {
		String st; 
		Hashtable<String, Smartphone> hashSmartphones = new Hashtable<>(); // Key: Nombre Celular, Value: Objeto Smartphone
		int i = 1;
		int fechas = 1; 
		while ((st = listaNombresCelulares.readLine()) != null) {
			Document d = Jsoup.connect("https://gadgets.ndtv.com/search?searchtext=" + st.replaceAll(" ", "+")).timeout(600000).get(); //Realiza una busqueda del nombre del celular almancenado en st
			if (d.select("div#productSearch") != null) { 
				Elements ele = d.select("div#productSearch"); //Selecciona los resultados de la busqueda
				for (Element element : ele.select("a.rvw-title")) { 
					
					if (!hashSmartphones.contains(element.text())) {
						Document c = Jsoup.connect(element.attr("href")).timeout(600000).get();
						System.out.println(element.text());
						if (!c.select("ul.pd_rating").isEmpty()) { 
							Elements ulRating = c.select("ul.pd_rating");
							Smartphone nuevo = new Smartphone(element.text(), null, null, element.attr("href"));
							Elements releaseDate = c.select("tr.firstrow");
							String trFechaLanzamiento = c.select("tr.firstrow").select("span").text();
							if (!trFechaLanzamiento.isEmpty()) {
								nuevo.setFechaLanzamiento(dateParser(trFechaLanzamiento));
								fechas++;
								nuevo.setPuntajes(Integer.parseInt(ulRating.select("li").get(0).select("li > i").attr("class").replace("sprite rating", "")), Integer.parseInt(ulRating.select("li").get(1).select("li > i").attr("class").replace("sprite rating", "")), Integer.parseInt(ulRating.select("li").get(2).select("li > i").attr("class").replace("sprite rating", "")), Integer.parseInt(ulRating.select("li").get(3).select("li > i").attr("class").replace("sprite rating", "")), Integer.parseInt(ulRating.select("li").get(4).select("li > i").attr("class").replace("sprite rating", "")), Integer.parseInt(ulRating.select("li").get(5).select("li > i").attr("class").replace("sprite rating", "")));							
								hashSmartphones.put(element.text(), nuevo);
								
								i++;
							}

						}
					}
				}
			}
		}
		System.out.println("Numero Celulares: " + i);
		System.out.println("Numero Fechas: " + fechas);
		System.out.println("  ");
		System.out.println(" Lista Celulares Hash: ");
		System.out.println(hashSmartphones.toString());
		ArrayList<String> listaCelulares = new ArrayList<>();
		for (String nombreDispositivo: hashSmartphones.keySet()) {
			listaCelulares.add(hashSmartphones.get(nombreDispositivo).toString());
		}
		lA.generarBaseDeDatos(listaCelulares);
		
	}
	
	
	/*public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {
		admArchivo lA = new admArchivo(); 
//		generarBaseDeDatos(lA.getListaCelulares(), lA);
		
//		obtenerPrecioUsadoYNuevo(lA.getListaCelulares());
		
	}*/
	
	public void obtenerPrecioUsadoYNuevo (BufferedReader listaNombresCelulares) throws IOException {
		String st; 
		while ((st = listaNombresCelulares.readLine()) != null) {
			Smartphone x = new Smartphone(st, null, null, null);
			obtenerPrecioUsado(x);
			obtenerPrecioNuevo(x);
		}
		System.out.println("Precios nuevos: " + numeroPrecioNuevo + "Precios usado: " + numeroPrecioUsado);
	}
	
	public boolean obtenerPrecioUsado (Smartphone phone) throws IOException{
		float precioUsado = 0;
		boolean valorEncontrado = false; 
		Document d = Jsoup.connect("https://camelcamelcamel.com/search?sq=" + phone.getNombre().replaceAll(" ", "+")).timeout(600000).get();
		if (d.select("div#products_list") != null){
			Elements elem = d.select("div#products_list").select("tr");
			Element e = elem.get(0);
			
			if (!e.select("div.price_used").text().equals("Not in Stock")) {
				precioUsado = Float.parseFloat(e.select("div.price_used").select("span.green").text().replace("$", "").replace(",", ""));
				numeroPrecioUsado++;
				valorEncontrado = true;				
			}
			System.out.println(phone.getNombre() + " Precio Usado: " + precioUsado);
			phone.setPrecioUsado(precioUsado);
			
		}
		return valorEncontrado;
	}
	
	public boolean hayConexion() {
		try {
			Document d = Jsoup.connect("https://camelcamelcamel.com").timeout(600000).get();
			return true;
		} catch (Exception e) {
			//System.out.println("No hay internet");
			return false;
		}

	}
	
	public void obtenerPrecioNuevo (Smartphone phone) throws IOException{ //SETEA EL PRECIO DE NUEVO EN CASO DE ENCONTRARLO. 
		float precioNuevo = 0;
		Document d = Jsoup.connect("https://camelcamelcamel.com/search?sq=" + phone.getNombre().replaceAll(" ", "+")).timeout(600000).get();

		
		if (d.select("div#products_list").toString() != ""){
			if (!d.select("div#products_list").select("tr").isEmpty()) {
				Elements elem = d.select("div#products_list").select("tr");
				Element e = elem.get(0);
				
				if (!e.select("div.price_new").text().equals("Not in Stock")) {
					precioNuevo = Float.parseFloat(e.select("div.price_new").select("span.green").text().replace("$", "").replace(",", ""));
					numeroPrecioNuevo++;
					if (precioNuevo > 60)
						phone.setPrecioNuevo(precioNuevo);
				}
				else
					if (!e.select("div.price_amazon").text().equals("Not in Stock")) {
						precioNuevo = Float.parseFloat(e.select("div.price_amazon").select("span.green").text().replace("$", "").replace(",", ""));
						numeroPrecioNuevo++;
						if (precioNuevo > 60)
							phone.setPrecioNuevo(precioNuevo);
					}	
			}
		
		}				
	}
	
}
