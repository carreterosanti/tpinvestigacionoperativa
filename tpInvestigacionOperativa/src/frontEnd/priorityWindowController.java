package frontEnd;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Set;

import org.jsoup.Jsoup;

import AnalyticAHP.Analityc;
import AnalyticAHP.Smartphone;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import webScrap.JsoupRun;

public class priorityWindowController extends Application implements Initializable {
	
    @FXML private AnchorPane topWindow;
    @FXML private ImageView btnClose;
    @FXML private ImageView btnMinimize;
    @FXML private ImageView btnBack;
    @FXML private ImageView btnNext;
    @FXML private ImageView btnRandom;
    @FXML private ImageView btnDelete;
    @FXML private Label bottomLabel = new Label();
    private Task checkNewPriceWorker;
    @FXML private Rectangle colorDesignVsScreen;
    @FXML private Text nroDesignVsScreen;
    @FXML private Text txtDesignVsScreen;
    @FXML private Rectangle colorScreenVsDesign;
    @FXML private Text nroScreenVsDesign;
    @FXML private Rectangle colorDesignVsCamera;
    @FXML private Text nroDesignVsCamera;
    @FXML private Text txtDesignVsCamera;
    @FXML private Rectangle colorCameraVsDesign;
    @FXML private Text nroCameraVsDesign;
    @FXML private Rectangle colorDesignVsSpeed;
    @FXML private Text nroDesignVsSpeed;
    @FXML private Text txtDesignVsSpeed;
    @FXML private Rectangle colorSpeedVsDesign;
    @FXML private Text nroSpeedVsDesign;
    @FXML private Rectangle colorDesignVsSoftware;
    @FXML private Text nroDesignVsSoftware;
    @FXML private Text txtDesignVsSoftware;
    @FXML private Rectangle colorSoftwareVsDesign;
    @FXML private Text nroSoftwareVsDesign;
    @FXML private Rectangle colorDesignVsBattery;
    @FXML private Text nroDesignVsBattery;
    @FXML private Text txtDesignVsBattery;
    @FXML private Rectangle colorBatteryVsDesign;
    @FXML private Text nroBatteryVsDesign;
    @FXML private Rectangle colorScreenVsCamera;
    @FXML private Text nroScreenVsCamera;
    @FXML private Text txtScreenVsCamera;
    @FXML private Rectangle colorCameraVsScreen;
    @FXML private Text nroCameraVsScreen;
    @FXML private Rectangle colorScreenVsSpeed;
    @FXML private Text nroScreenVsSpeed;
    @FXML private Text txtScreenVsSpeed;
    @FXML private Rectangle colorSpeedVsScreen;
    @FXML private Text nroSpeedVsScreen;
    @FXML private Rectangle colorScreenVsSoftware;
    @FXML private Text nroScreenVsSoftware;
    @FXML private Text txtScreenVsSoftware;
    @FXML private Rectangle colorSoftwareVsScreen;
    @FXML private Text nroSoftwareVsScreen;
    @FXML private Rectangle colorScreenVsBattery;
    @FXML private Text nroScreenVsBattery;
    @FXML private Text txtScreenVsBattery;
    @FXML private Rectangle colorBatteryVsScreen;
    @FXML private Text nroBatteryVsScreen;
    @FXML private Rectangle colorCameraVsSpeed;
    @FXML private Text nroCameraVsSpeed;
    @FXML private Text txtCameraVsSpeed;
    @FXML private Rectangle colorSpeedVsCamera;
    @FXML private Text nroSpeedVsCamera;
    @FXML private Rectangle colorCameraVsSoftware;
    @FXML private Text nroCameraVsSoftware;
    @FXML private Text txtCameraVsSoftware;
    @FXML private Rectangle colorSoftwareVsCamera;
    @FXML private Text nroSoftwareVsCamera;
    @FXML private Rectangle colorCameraVsBattery;
    @FXML private Text nroCameraVsBattery;
    @FXML private Text txtCameraVsBattery;
    @FXML private Rectangle colorBatteryVsCamera;
    @FXML private Text nroBatteryVsCamera;
    @FXML private Rectangle colorSpeedVsSoftware;
    @FXML private Text nroSpeedVsSoftware;
    @FXML private Text txtSpeedVsSoftware;
    @FXML private Rectangle colorSoftwareVsSpeed;
    @FXML private Text nroSoftwareVsSpeed;
    @FXML private Rectangle colorSpeedVsBattery;
    @FXML private Text nroSpeedVsBattery;
    @FXML private Text txtSpeedVsBattery;
    @FXML private Rectangle colorBatteryVsSpeed;
    @FXML private Text nroBatteryVsSpeed;
    @FXML private Rectangle colorSoftwareVsBattery;
    @FXML private Text nroSoftwareVsBattery;
    @FXML private Text txtSoftwareVsBattery;
    @FXML private Rectangle colorBatteryVsSoftware;
    @FXML private Text nroBatteryVsSoftware;
    @FXML private ImageView imgVs_;
    @FXML private Text txtVs_;
    @FXML private ImageView imgVs;
    @FXML private Text txtVs;
    @FXML private Circle scoreOut_5;
    @FXML private Circle scoreCenter_5;
    @FXML private Circle scoreOut_4;
    @FXML private Circle scoreCenter_4;
    @FXML private Circle scoreOut_3;
    @FXML private Circle scoreCenter_3;
    @FXML private Circle scoreOut_2;
    @FXML private Circle scoreCenter_2;
    @FXML private Circle scoreOut_1;
    @FXML private Circle scoreCenter_1;
    @FXML private Circle scoreOut0;
    @FXML private Circle scoreCenter0;
    @FXML private Circle scoreOut1;
    @FXML private Circle scoreCenter1;
    @FXML private Circle scoreOut2;
    @FXML private Circle scoreCenter2;
    @FXML private Circle scoreOut3;
    @FXML private Circle scoreCenter3;
    @FXML private Circle scoreOut4;
    @FXML private Circle scoreCenter4;
    @FXML private Circle scoreOut5;
    @FXML private Circle scoreCenter5;
    
    private Image imgDesign; 
    private Image imgScreen; 
    private Image imgCamera;
    private Image imgSpeed;
    private Image imgSoftware; 
    private Image imgBattery; 
 
    private List<priorityScoreView> listaVs = new ArrayList<>(); 
    private int indiceVs = 0; 
   
    private double xOffset = 0; // xOffset e yOffset permiten almacenar la posicion de la ventana al ser desplazada por el usuario
    private double yOffset = 0; 
    private boolean styled = false; // Permite verificar si ya fue removido el marco de la ventana. 
    private Stage sStage = new Stage();
    private Stage tStage = new Stage();
    private Stage fStage = new Stage();
    
    private Analityc analityc;
    private investmentWindowController previusController; 
    private ArrayList<String> listaActividad = new ArrayList<>();
    private ArrayList<String> listaCriterios = new ArrayList<>(); 
    
    private JsoupRun offlineFlag = new JsoupRun();
	
	public priorityWindowController(Stage sStage, Stage tStage, Analityc analityc, investmentWindowController previusController) {
		this.tStage = tStage; 		
		this.sStage = sStage;
		this.analityc = analityc;
		this.previusController = previusController; 
	}
	
	// SIGUIENTE VENTANA resultWindow mediante btnNext
	public void nextResultWindow() {
		
		List<Object> listaAlternativas = new ArrayList<>();
		listaAlternativas.addAll(analityc.getHashSmartphones().values());
		
		if (offlineFlag.hayConexion()) {
			// FILTRAR LISTA SEGUN VALOR DE INVERSION
			analityc.getAhp_Smartphone().setListaAlternativas(Smartphone.filtroPrecioMenor(listaAlternativas, analityc.getValorTotalInversion()));
			System.out.println("Numero Alternativas: " + analityc.getAhp_Smartphone().getListaAlternativas().size());

			// FILTRAR LISTA SEGUN FECHA DE LANZAMIENTO
			analityc.getAhp_Smartphone().setListaAlternativas(Smartphone.filtroPorFecha(analityc.getAhp_Smartphone().getListaAlternativas(), analityc.getDispositivoElegido().getFechaLanzamiento()));
			
		} else {
			analityc.getAhp_Smartphone().setListaAlternativas(listaAlternativas);
		}

		System.out.println("Numero Alternativas: " + analityc.getAhp_Smartphone().getListaAlternativas().size());

		
		try {
				xOffset = tStage.getX();
	            yOffset = tStage.getY();
	            
	    		if (analityc.getGrupoThread().activeCount() == 0) {
					this.start(fStage);
					tStage.hide();
	    		} else {
	    			bottomLabel.setVisible(true);
	    			new Thread(checkNewPriceWorker).start();	 
	    		}
	    			
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	public int getIndiceVs() {
		return indiceVs;
	}
	
	public Analityc getAnalityc() {
		return analityc;
	}
	
	
	
	@Override
	public void start(Stage fourthStage) throws Exception {
		
		FXMLLoader loader = new FXMLLoader(getClass().getResource("resultWindow.fxml"));
		resultWindowController c = new resultWindowController(tStage, fourthStage, analityc); 
		loader.setController(c);
		Pane mainPane = (Pane) loader.load();
		Scene scene = new Scene(mainPane);
		fourthStage.setScene(scene);
		fourthStage.setX(xOffset);
		fourthStage.setY(yOffset);
		if (!styled) {
			fourthStage.initStyle(StageStyle.UNDECORATED);
			styled = true;
		}
		fourthStage.setWidth(tStage.getWidth());
		fourthStage.setHeight(tStage.getHeight());
		fourthStage.show();
		mainPane.requestFocus();
		
	}
	
	public priorityScoreView setNextVs() {
		if (indiceVs < listaVs.size()-1) {
			priorityScoreView nextVs = listaVs.get(indiceVs+1);
			imgVs_.setImage(nextVs.getImg1());
			imgVs.setImage(nextVs.getImg2());
			txtVs_.setText(nextVs.getVs1());
			txtVs.setText(nextVs.getVs2());
		}
		return null;
	}
	
	public void desactivarBotoneraScore() {
		Paint transparente = Paint.valueOf("#ffffff00");
		Paint blanco = Paint.valueOf("#ffffff");
		scoreCenter0.setStroke(transparente);
		scoreCenter0.setFill(transparente);
		scoreCenter1.setStroke(transparente);
		scoreCenter1.setFill(transparente);
		scoreCenter2.setStroke(transparente);
		scoreCenter2.setFill(transparente);
		scoreCenter3.setStroke(transparente);
		scoreCenter3.setFill(transparente);
		scoreCenter4.setStroke(transparente);
		scoreCenter4.setFill(transparente);
		scoreCenter5.setStroke(transparente);
		scoreCenter5.setFill(transparente);
		scoreCenter_1.setStroke(transparente);
		scoreCenter_1.setFill(transparente);
		scoreCenter_2.setStroke(transparente);
		scoreCenter_2.setFill(transparente);
		scoreCenter_3.setStroke(transparente);
		scoreCenter_3.setFill(transparente);
		scoreCenter_4.setStroke(transparente);
		scoreCenter_4.setFill(transparente);
		scoreCenter_5.setStroke(transparente);
		scoreCenter_5.setFill(transparente);
		scoreOut0.setStroke(blanco);
		scoreOut0.setStroke(blanco);
		scoreOut1.setStroke(blanco);
		scoreOut2.setStroke(blanco);
		scoreOut3.setStroke(blanco);
		scoreOut4.setStroke(blanco);
		scoreOut5.setStroke(blanco);
		scoreOut_1.setStroke(blanco);
		scoreOut_2.setStroke(blanco);
		scoreOut_3.setStroke(blanco);
		scoreOut_4.setStroke(blanco);
		scoreOut_5.setStroke(blanco);
	}
	
	public void showPriotityScore(priorityScoreView pS, int iVs) {
		imgVs_.setImage(pS.getImg1());
		imgVs.setImage(pS.getImg2());
		txtVs_.setText(pS.getVs1());
		txtVs.setText(pS.getVs2());
		indiceVs = iVs;
		desactivarBotoneraScore();
		if (!pS.getNroVs2().getText().equals("X")) {
			switch (pS.getNroVs2().getText()) {
			case "":
				scoreOut0.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter0.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter0.setFill(Paint.valueOf("#1ABC9C"));
				break;
			case "1":
				scoreOut1.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter1.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter1.setFill(Paint.valueOf("#1ABC9C"));
				break;
			case "2":
				scoreOut2.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter2.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter2.setFill(Paint.valueOf("#1ABC9C"));
				break;
			case "3":
				scoreOut3.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter3.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter3.setFill(Paint.valueOf("#1ABC9C"));
				break;
			case "4":
				scoreOut4.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter4.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter4.setFill(Paint.valueOf("#1ABC9C"));
				break;
			case "5":
				scoreOut5.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter5.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter5.setFill(Paint.valueOf("#1ABC9C"));
				break;
			}
		} else if (!pS.getNroVs1().getText().equals("X")) {
			switch (pS.getNroVs1().getText()) {
			case "":
				scoreOut0.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter0.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter0.setFill(Paint.valueOf("#1ABC9C"));
				break;
			case "1":
				scoreOut_1.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter_1.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter_1.setFill(Paint.valueOf("#1ABC9C"));
				break;
			case "2":
				scoreOut_2.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter_2.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter_2.setFill(Paint.valueOf("#1ABC9C"));
				break;
			case "3":
				scoreOut_3.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter_3.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter_3.setFill(Paint.valueOf("#1ABC9C"));
				break;
			case "4":
				scoreOut_4.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter_4.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter_4.setFill(Paint.valueOf("#1ABC9C"));
				break;
			case "5":
				scoreOut_5.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter_5.setStroke(Paint.valueOf("#1ABC9C"));
				scoreCenter_5.setFill(Paint.valueOf("#1ABC9C"));
				break;
			}
		}
	}
 
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		// Inicializacion del thread para checkear precios nuevos listos. 
		checkNewPriceWorker = checkNewPricesEndWorker();
		
		// Inicialiacion Imagenes Vs
        File file = new File("Imagenes/DesignIcon.png");
        imgDesign = new Image(file.toURI().toString());
        file = new File("Imagenes/ScreenIcon.png");
        imgScreen = new Image(file.toURI().toString());
        file = new File("Imagenes/CameraIcon.png");
        imgCamera = new Image(file.toURI().toString());
        file = new File("Imagenes/SpeedIcon.png");
        imgSpeed = new Image(file.toURI().toString());
        file = new File("Imagenes/SoftwareIcon.png");
        imgSoftware = new Image(file.toURI().toString());
        file = new File("Imagenes/BatteryIcon.png");
        imgBattery = new Image(file.toURI().toString());
		
		// Inicializacion lista de Vs
		listaVs.add(new priorityScoreView(txtDesignVsScreen, colorDesignVsScreen, colorScreenVsDesign, nroDesignVsScreen, nroScreenVsDesign, this, imgDesign, imgScreen, "Dise�o", "Pantalla"));
		listaVs.add(new priorityScoreView(txtDesignVsCamera, colorDesignVsCamera, colorCameraVsDesign, nroDesignVsCamera, nroCameraVsDesign, this, imgDesign, imgCamera, "Dise�o", "C�mara"));
		listaVs.add(new priorityScoreView(txtDesignVsSpeed, colorDesignVsSpeed, colorSpeedVsDesign, nroDesignVsSpeed, nroSpeedVsDesign, this, imgDesign, imgSpeed, "Dise�o", "Rendimiento"));
		listaVs.add(new priorityScoreView(txtDesignVsSoftware, colorDesignVsSoftware, colorSoftwareVsDesign, nroDesignVsSoftware, nroSoftwareVsDesign, this, imgDesign, imgSoftware, "Dise�o", "Software"));
		listaVs.add(new priorityScoreView(txtDesignVsBattery, colorDesignVsBattery, colorBatteryVsDesign, nroDesignVsBattery, nroBatteryVsDesign, this, imgDesign, imgBattery, "Dise�o", "Bater�a"));
		listaVs.add(new priorityScoreView(txtScreenVsCamera, colorScreenVsCamera, colorCameraVsScreen, nroScreenVsCamera, nroCameraVsScreen, this, imgScreen, imgCamera, "Pantalla", "C�mara"));
		listaVs.add(new priorityScoreView(txtScreenVsSpeed, colorScreenVsSpeed, colorSpeedVsScreen, nroScreenVsSpeed, nroSpeedVsScreen, this, imgScreen, imgSpeed, "Pantalla", "Rendimiento"));
		listaVs.add(new priorityScoreView(txtScreenVsSoftware, colorScreenVsSoftware, colorSoftwareVsScreen, nroScreenVsSoftware, nroSoftwareVsScreen, this, imgScreen, imgSoftware, "Pantalla", "Software"));
		listaVs.add(new priorityScoreView(txtScreenVsBattery, colorScreenVsBattery, colorBatteryVsScreen, nroScreenVsBattery, nroBatteryVsScreen, this, imgScreen, imgBattery, "Pantalla", "Bater�a"));
		listaVs.add(new priorityScoreView(txtCameraVsSpeed, colorCameraVsSpeed, colorSpeedVsCamera, nroCameraVsSpeed, nroSpeedVsCamera, this, imgCamera, imgSpeed, "C�mara", "Rendimiento"));
		listaVs.add(new priorityScoreView(txtCameraVsSoftware, colorCameraVsSoftware, colorSoftwareVsCamera, nroCameraVsSoftware, nroSoftwareVsCamera, this, imgCamera, imgSoftware, "C�mara", "Software"));
		listaVs.add(new priorityScoreView(txtCameraVsBattery, colorCameraVsBattery, colorBatteryVsCamera, nroCameraVsBattery, nroBatteryVsCamera, this, imgCamera, imgBattery, "C�mara", "Bater�a"));
		listaVs.add(new priorityScoreView(txtSpeedVsSoftware, colorSpeedVsSoftware, colorSoftwareVsSpeed, nroSpeedVsSoftware, nroSoftwareVsSpeed, this, imgSpeed, imgSoftware, "Rendimiento", "Software"));
		listaVs.add(new priorityScoreView(txtSpeedVsBattery, colorSpeedVsBattery, colorBatteryVsSpeed, nroSpeedVsBattery, nroBatteryVsSpeed, this, imgSpeed, imgBattery, "Rendimiento", "Bater�a"));
		listaVs.add(new priorityScoreView(txtSoftwareVsBattery, colorSoftwareVsBattery, colorBatteryVsSoftware, nroSoftwareVsBattery, nroBatteryVsSoftware, this, imgSoftware, imgBattery, "Software", "Bater�a"));
		
		
		
		// BOTONES 5-4-3-2-1-0-1-2-3-4-5 
		scoreOut_1.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 if (indiceVs < listaVs.size()) {
		    		 listaVs.get(indiceVs).selectScore(-1);
		    	 	indiceVs++;
		    	 }	 
		     }
		});
		
		scoreOut_2.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 if (indiceVs < listaVs.size()) {
		    		 listaVs.get(indiceVs).selectScore(-2);
		    	 	indiceVs++;
		    	 }	 
		     }
		});
		
		scoreOut_3.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 if (indiceVs < listaVs.size()) {
		    		 listaVs.get(indiceVs).selectScore(-3);
		    	 	indiceVs++;
		    	 }	 
		     }
		});
		
		scoreOut_4.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 if (indiceVs < listaVs.size()) {
		    		 listaVs.get(indiceVs).selectScore(-4);
		    	 	indiceVs++;
		    	 }	 
		     }
		});
		
		scoreOut_5.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 if (indiceVs < listaVs.size()) {
		    		 listaVs.get(indiceVs).selectScore(-5);
		    	 	indiceVs++;
		    	 }	 
		     }
		});
		
		scoreOut1.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 if (indiceVs < listaVs.size()) {
		    		 listaVs.get(indiceVs).selectScore(1);
		    	 	indiceVs++;
		    	 }	 
		     }
		});
		
		scoreOut2.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 if (indiceVs < listaVs.size()) {
		    		 listaVs.get(indiceVs).selectScore(2);
		    	 	indiceVs++;
		    	 }	 
		     }
		});
		
		scoreOut3.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 if (indiceVs < listaVs.size()) {
		    		 listaVs.get(indiceVs).selectScore(3);
		    	 	indiceVs++;
		    	 }	 
		     }
		});
		
		scoreOut4.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 if (indiceVs < listaVs.size()) {
		    		 listaVs.get(indiceVs).selectScore(4);
		    	 	indiceVs++;
		    	 }	 
		     }
		});
		
		scoreOut5.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 if (indiceVs < listaVs.size()) {
		    		 listaVs.get(indiceVs).selectScore(5);
		    	 	indiceVs++;
		    	 }	 
		     }
		});
		
		scoreOut0.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 if (indiceVs < listaVs.size()) {
		    		 listaVs.get(indiceVs).selectScore(0);
		    	 	indiceVs++;
		    	 }	 
		     }
		});
		
		scoreCenter_1.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 if (indiceVs < listaVs.size()) {
		    		 listaVs.get(indiceVs).selectScore(-1);
		    	 	indiceVs++;
		    	 }	 
		     }
		});
		scoreCenter_2.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 if (indiceVs < listaVs.size()) {
		    		 listaVs.get(indiceVs).selectScore(-2);
		    	 	indiceVs++;
		    	 }
		     }
		});
		scoreCenter_3.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 if (indiceVs < listaVs.size()) {
		    		 listaVs.get(indiceVs).selectScore(-3);
		    	 	indiceVs++;
		    	 }
		     }
		});
		scoreCenter_4.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 if (indiceVs < listaVs.size()) {
		    		 listaVs.get(indiceVs).selectScore(-4);
		    	 	indiceVs++;
		    	 }
		     }
		});
		scoreCenter_5.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 if (indiceVs < listaVs.size()) {
		    		 listaVs.get(indiceVs).selectScore(-5);
		    	 	indiceVs++;
		    	 }
		     }
		});
		scoreCenter0.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 if (indiceVs < listaVs.size()) {
		    		 listaVs.get(indiceVs).selectScore(0);
		    	 	indiceVs++;
		    	 }
		     }
		});
		scoreCenter1.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 if (indiceVs < listaVs.size()) {
		    		 listaVs.get(indiceVs).selectScore(1);
		    	 	indiceVs++;
		    	 }
		     }
		});
		scoreCenter2.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 if (indiceVs < listaVs.size()) {
		    		 listaVs.get(indiceVs).selectScore(2);
		    	 	indiceVs++;
		    	 }
		     }
		});
		scoreCenter3.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 if (indiceVs < listaVs.size()) {
		    		 listaVs.get(indiceVs).selectScore(3);
		    	 	indiceVs++;
		    	 }
		     }
		});
		scoreCenter4.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 if (indiceVs < listaVs.size()) {
		    		 listaVs.get(indiceVs).selectScore(4);
		    	 	indiceVs++;
		    	 }
		     }
		});
		scoreCenter5.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 if (indiceVs < listaVs.size()) {
		    		 listaVs.get(indiceVs).selectScore(5);
		    	 	indiceVs++;
		    	 }
		     }
		});
		
		//VISTAS VS
		txtDesignVsScreen.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 showPriotityScore(listaVs.get(0),0);
		     }
		});
		txtDesignVsCamera.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 showPriotityScore(listaVs.get(1),1);
		     }
		});
		txtDesignVsSpeed.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 showPriotityScore(listaVs.get(2),2);
		     }
		});
		txtDesignVsSoftware.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 showPriotityScore(listaVs.get(3),3);
		     }
		});
		txtDesignVsBattery.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 showPriotityScore(listaVs.get(4),4);
		     }
		});
		txtScreenVsCamera.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 showPriotityScore(listaVs.get(5),5);
		     }
		});
		txtScreenVsSpeed.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 showPriotityScore(listaVs.get(6),6);
		     }
		});
		txtScreenVsSoftware.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 showPriotityScore(listaVs.get(7),7);
		     }
		});
		txtScreenVsBattery.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 showPriotityScore(listaVs.get(8),8);
		     }
		});
		txtCameraVsSpeed.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 showPriotityScore(listaVs.get(9),9);
		     }
		});
		txtCameraVsSoftware.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 showPriotityScore(listaVs.get(10),10);
		     }
		});
		txtCameraVsBattery.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 showPriotityScore(listaVs.get(11),11);
		     }
		});
		txtSpeedVsSoftware.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 showPriotityScore(listaVs.get(12),12);
		     }
		});
		txtSpeedVsBattery.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 showPriotityScore(listaVs.get(13),13);
		     }
		});
		txtSoftwareVsBattery.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		     @Override
		     public void handle(MouseEvent event) {
		    	 showPriotityScore(listaVs.get(14),14);
		     }
		});

		
		// ELIMINAR SELECCION PRIORIDAD DESDE BOTON DELETE
		btnDelete.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                // DESACTIVA IGUALDAD
                File file = new File("Imagenes\\EqualIcon.png"); 
                Image image = new Image(file.toURI().toString());
        		listaActividad.clear();
        		desactivarBotoneraScore();
        		indiceVs=0; 
        		listaVs.get(0).selectScore(indiceVs);
        		System.out.println("Boton Cancelar inicia con: " + listaVs.get(0).getVs1() + " " + listaVs.get(0).getVs2());
        		for (priorityScoreView vs: listaVs) {
        			vs.deleteScores();
        		}
                
            }
        });	
		
		
		btnRandom.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                // DESACTIVA IGUALDAD
            	Random r = new Random(); 
            	int min= -5; 
            	int max= 6;
        		for (priorityScoreView vs: listaVs) {
        			vs.selectScore(r.nextInt(max-min) + min);
        			indiceVs++;
        		}
                
            }
        });	
		
    	// MOVIMIENTO DE VENTANA DESDE TOP WINDOWS
		topWindow.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });	
		
		topWindow.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                tStage.setX(event.getScreenX() - xOffset);
                tStage.setY(event.getScreenY() - yOffset);
            }
        });
		
		// CERRAR VENTANA MEDIANTE BOTON CLOSE
		btnClose.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

		     @Override
		     public void handle(MouseEvent event) {
		         tStage.close();		         
		         event.consume();
		     }
		});
		
		// MINIMIZAR VENTANA MEDIANTE BOTON MINIMIZE
		btnMinimize.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

		     @Override
		     public void handle(MouseEvent event) {
		         tStage.setIconified(true);	         
		         event.consume();
		     }
		});
		
		// RETROCEDER A MAINWINDOW MEDIANTE BOTON BACK
		btnBack.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

		     @Override
		     public void handle(MouseEvent event) {
		    	 sStage.setX(event.getScreenX() - xOffset);
		    	 sStage.setY(event.getScreenY() - yOffset);
		    	 sStage.show();
		         tStage.hide();	
		         previusController.backFromPriorityWindow();
		         event.consume();
		     }
		});
		
	}
	
	public Task checkNewPricesEndWorker() {
    	return new Task() {

			@Override
			protected Object call() throws Exception {
				while (analityc.getGrupoThread().activeCount() != 0) {
					Thread.sleep(1000);
				}
				Platform.runLater( // "Platform.runLater" PERMITE OMITIR EL WARNING EN EJECUCION YA QUE SE ESTA EDITANDO EL TEXTO DESDE OTRO THREAD
						  () -> {
								bottomLabel.setText("Actualizacion de Precios Lista.");
								bottomLabel.setStyle("-fx-background-color: #00C853;");								  }
						);
				return null;
			}
    	};
	}



}
