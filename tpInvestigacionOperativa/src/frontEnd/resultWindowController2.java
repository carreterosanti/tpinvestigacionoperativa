package frontEnd;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import AnalyticAHP.Analityc;
import AnalyticAHP.Smartphone;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import webScrap.JsoupRun;

public class resultWindowController2 implements Initializable{

	
    @FXML private AnchorPane topWindow;
    @FXML private ImageView btnClose;
    @FXML private ImageView btnMinimize;
    @FXML private ImageView btnBack;
    @FXML private Stage tStage;
    @FXML private Stage fStage;
    @FXML private Text nameTop2;
    @FXML private Text nameTop1;
    @FXML private Text nameTop3;
    @FXML private Text nameDisplay;
    @FXML private Text nroDesign;
    @FXML private Text nroScreen;
    @FXML private Text nroSoftware;
    @FXML private Text nroSpeed;
    @FXML private Text nroBattery;
    @FXML private Text nroCamera;
    @FXML private Text fechaLanzamiento;
    @FXML private Text valorSmartphone;
    @FXML private ImageView imgSmartphoneSelect;
    
    private double xOffset = 0; // xOffset e yOffset permiten almacenar la posicion de la ventana al ser desplazada por el usuario
    private double yOffset = 0; 
    private boolean styled = false; // Permite verificar si ya fue removido el marco de la ventana. 
	
	private Analityc analityc; 
	private List<Object> listaTop;
	
    private JsoupRun imageSmartPhone = new JsoupRun();
    private Task getImageWorker; 
    private Smartphone smartphoneSeleccionado = null;
    
    public resultWindowController2(Stage tStage, Stage fStage, Analityc analityc) {
		this.tStage = tStage; 
		this.fStage = fStage;
		this.analityc = analityc;
	}
	
    public Smartphone showSelectSmartphone(Smartphone smartphone) {
    	nameDisplay.setText(smartphone.getNombre());
    	nroDesign.setText(smartphone.getDesign() + "");
    	nroScreen.setText(smartphone.getScreen() + "");
    	nroSoftware.setText(smartphone.getSoftware() + "");
    	nroSpeed.setText(smartphone.getSpeed() + "");
    	nroBattery.setText(smartphone.getBattery() + "");
    	nroCamera.setText(smartphone.getCamera()+ "");
    	fechaLanzamiento.setText(smartphone.getFechaLanzamiento().toString());
    	valorSmartphone.setText("Valor: $" + smartphone.getPrecioNuevo());
    	return smartphone;
    }
    
    public void showTop1() {
    	smartphoneSeleccionado = showSelectSmartphone((Smartphone) listaTop.get(0));
    	getImageWorker = createWorker();
    	new Thread(getImageWorker).start();
    	
    }
    
    public void showTop2() {
    	smartphoneSeleccionado = showSelectSmartphone((Smartphone) listaTop.get(1));
    	getImageWorker = createWorker();
    	new Thread(getImageWorker).start();
    }
    
    public void showTop3() {
    	smartphoneSeleccionado = showSelectSmartphone((Smartphone) listaTop.get(2));
    	getImageWorker = createWorker();
    	new Thread(getImageWorker).start();
    }
    
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
    	// REALIZAR ANALISIS AHP
		listaTop = analityc.getAhp_Smartphone().generarAnalisisAHP();
		
		showTop1();
		
		// SETTEAR TOP
    	if (listaTop.size() >= 3) {
    		Smartphone top1 = (Smartphone) listaTop.get(0); 
        	Smartphone top2 = (Smartphone) listaTop.get(1); 
        	Smartphone top3 = (Smartphone) listaTop.get(2);
    	
        	nameTop1.setText(top1.getNombre());
        	nameTop2.setText(top2.getNombre());
        	nameTop3.setText(top3.getNombre());
    	}

		
		// MOVIMIENTO DE VENTANA DESDE TOP WINDOWS
		topWindow.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });	
		
		topWindow.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
            	fStage.setX(event.getScreenX() - xOffset);
            	fStage.setY(event.getScreenY() - yOffset);
            }
        });
		
		// CERRAR VENTANA MEDIANTE BOTON CLOSE
		btnClose.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

		     @Override
		     public void handle(MouseEvent event) {
		    	 fStage.close();		         
		         event.consume();
		     }
		});
		
		// MINIMIZAR VENTANA MEDIANTE BOTON MINIMIZE
		btnMinimize.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

		     @Override
		     public void handle(MouseEvent event) {
		    	 fStage.setIconified(true);	         
		         event.consume();
		     }
		});
		
		// RETROCEDER A MAINWINDOW MEDIANTE BOTON BACK
		btnBack.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

		     @Override
		     public void handle(MouseEvent event) {
		    	 tStage.setX(event.getScreenX() - xOffset);
		    	 tStage.setY(event.getScreenY() - yOffset);
		    	 tStage.show();
		         fStage.hide();		         
		         event.consume();
		     }
		});
		
	}
	
    public synchronized Task createWorker() {
    	return new Task() {

			@Override
			protected Object call() throws Exception {
				if (imageSmartPhone.getImageURL(smartphoneSeleccionado) != null) {
					URL url = new URL(imageSmartPhone.getImageURL(smartphoneSeleccionado));
	        		Image image = new Image(url.toString());
	                Image img = imgSmartphoneSelect.getImage();
	                if (img != null) {
	                    double w = 0;
	                    double h = 0;

	                    double ratioX = imgSmartphoneSelect.getFitWidth() / img.getWidth();
	                    double ratioY = imgSmartphoneSelect.getFitHeight() / img.getHeight();
	                    
	                    double reducCoeff = 0;
	                    if(ratioX >= ratioY) {
	                        reducCoeff = ratioY;
	                    } else {
	                        reducCoeff = ratioX;
	                    }

	                    w = img.getWidth() * reducCoeff;
	                    h = img.getHeight() * reducCoeff;

	                    imgSmartphoneSelect.setX((imgSmartphoneSelect.getFitWidth() - w) / 2);
	                    imgSmartphoneSelect.setY((imgSmartphoneSelect.getFitHeight() - h) / 2);
	                }
	            	updateProgress(100, 100);
	            	imgSmartphoneSelect.setImage(image);
	            	imgSmartphoneSelect.setVisible(true);
					return true;
				}
				return true;

			}
    		
    	};
    }
	
	

	
	
	
}
