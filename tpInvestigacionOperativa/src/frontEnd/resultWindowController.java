package frontEnd;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import AnalyticAHP.Analityc;
import AnalyticAHP.Smartphone;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import webScrap.JsoupRun;

public class resultWindowController implements Initializable{

	
    @FXML private AnchorPane topWindow;
    @FXML private ImageView btnClose;
    @FXML private ImageView btnMinimize;
    @FXML private ImageView btnBack;
    @FXML private Stage tStage;
    @FXML private Stage fStage;
    @FXML private Text nameTop2;
    @FXML private Text nameTop1;
    @FXML private Text nameTop3;
    @FXML private Text nameDisplay;
    @FXML private Text nroDesign;
    @FXML private Text nroScreen;
    @FXML private Text nroSoftware;
    @FXML private Text nroSpeed;
    @FXML private Text nroBattery;
    @FXML private Text nroCamera;
    @FXML private Text fechaLanzamiento;
    @FXML private Text valorSmartphone;
    @FXML private Text txtInversionTotal;
    @FXML private Text name50Top2;
    @FXML private Text name50Top1;
    @FXML private Text name50Top3;
    @FXML private Text name100Top2;
    @FXML private Text name100Top1;
    @FXML private Text name100Top3;
    @FXML private Text txtInversion50;
    @FXML private Text txtInversion100;
    @FXML private ImageView imgOfflineFlag;
    @FXML private Text scoreGlobal1;
    @FXML private Text scoreGlobal2;
    @FXML private Text scoreGlobal3;
    
    
    
    private double xOffset = 0; // xOffset e yOffset permiten almacenar la posicion de la ventana al ser desplazada por el usuario
    private double yOffset = 0; 
    private boolean styled = false; // Permite verificar si ya fue removido el marco de la ventana. 
	
	private Analityc analityc; 
    private JsoupRun offlineFlag = new JsoupRun();
	boolean online = offlineFlag.hayConexion();
	
	private topResultView viewTopResult; 
	private topResultView viewTop50; 
	private topResultView viewTop100; 
	
	private List<Object> listaTop;
	private List<Object> listaTop50 = new ArrayList<>();
	private List<Object> listaTop100 = new ArrayList<>();
    
    public resultWindowController(Stage tStage, Stage fStage, Analityc analityc) {
		this.tStage = tStage; 
		this.fStage = fStage;
		this.analityc = analityc;
	}
	
    public void showSelectSmartphone(Smartphone smartphone) {
    	nameDisplay.setText(smartphone.getNombre());
    	nroDesign.setText(smartphone.getDesign() + "");
    	nroScreen.setText(smartphone.getScreen() + "");
    	nroSoftware.setText(smartphone.getSoftware() + "");
    	nroSpeed.setText(smartphone.getSpeed() + "");
    	nroBattery.setText(smartphone.getBattery() + "");
    	nroCamera.setText(smartphone.getCamera()+ "");
    	fechaLanzamiento.setText(smartphone.getFechaLanzamiento().toString());
    	if (online) {
    		valorSmartphone.setText("Valor: $" + smartphone.getPrecioNuevo());
    	} else {
    		valorSmartphone.setText("Valor: -");
    	}
    	
    }

    
    public void showTop1() {
    	showSelectSmartphone((Smartphone) listaTop.get(0));
    }
    
    public void showTop2() {
    	showSelectSmartphone((Smartphone) listaTop.get(2));
    }
    
    public void showTop3() {
    	showSelectSmartphone((Smartphone) listaTop.get(4));
    }
    
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
    	// CHECK CONNECTION
		if (!online)
			imgOfflineFlag.setVisible(true);
		
		
		// SETEAR INVERSION TOTAL LABEL
		txtInversionTotal.setText("Inversion Total: $ " + analityc.getValorTotalInversion());
		
		// REALIZAR ANALISIS AHP
		listaTop = analityc.getAhp_Smartphone().generarAnalisisAHP();
		
		// IMPRESION DE CR PARA CADA MATRIZ INVOLUCRADA EN AHP
		//analityc.getAhp_Smartphone().getMatrizPrioridadCriterios().imprimirMatriz();
		analityc.getAhp_Smartphone().imprimirCocienteDeConsistenciaMatricesInvolucradas();
		
		if (!analityc.getAhp_Smartphone().isMatrizPrioridadCrioteriosConsistente()) {
			File file = new File("Imagenes/RedBackIcon.png");
			btnBack.setImage(new Image(file.toURI().toString()));
		}
		
		// REALIZAR ANALISIS AHP CON INVERSION+ E INVERSION++
		if (online)
			analityc.realizarAHPConPorcentajePresupuestos(listaTop50, listaTop100);
		
		// INICIALIZAR LOS topResultView
		if (listaTop.size() >= 6)
			viewTopResult = new topResultView(((Smartphone)listaTop.get(0)), (float) listaTop.get(1),((Smartphone)listaTop.get(2)), (float) listaTop.get(3),((Smartphone)listaTop.get(4)), (float) listaTop.get(5),("Inversion Indicada: $" + analityc.getValorTotalInversion()));
		if (listaTop50.size() >= 6)
			viewTop50 = new topResultView(((Smartphone)listaTop50.get(0)), ((Smartphone)listaTop50.get(2)), ((Smartphone)listaTop50.get(4)), ("Inversion Propuesta: $" + analityc.getValorInversionMas50Porc()));
		if (listaTop100.size() >= 6)
			viewTop100 = new topResultView(((Smartphone)listaTop100.get(0)), ((Smartphone)listaTop100.get(2)), ((Smartphone)listaTop100.get(4)), ("Inversion Propuesta: $" + analityc.getValorInverionDoble()));

		
		
		
		showTop1();
		
		// SETTEAR TOP RESULTADOS
    	if (listaTop.size() >= 6) {
    		Smartphone top1 = (Smartphone) listaTop.get(0); 
        	Smartphone top2 = (Smartphone) listaTop.get(2); 
        	Smartphone top3 = (Smartphone) listaTop.get(4);
    	
        	nameTop1.setText(top1.getNombre());
        	scoreGlobal1.setText(String.valueOf(viewTopResult.getScore1()));
        	nameTop2.setText(top2.getNombre());
        	scoreGlobal2.setText(String.valueOf(viewTopResult.getScore2()));
        	nameTop3.setText(top3.getNombre());
        	scoreGlobal3.setText(String.valueOf(viewTopResult.getScore3()));
        	
    	}
    	
    	if (online) {
        	// SETTEAR TOP 50%
        	txtInversion50.setText(viewTop50.getTituloInversion());
        	if (listaTop50.size() >= 6) {
        		name50Top1.setText(viewTop50.getTop1().getNombre());
        		name50Top2.setText(viewTop50.getTop2().getNombre());
        		name50Top3.setText(viewTop50.getTop3().getNombre());
        	}
        	
        	// SETTEAR TOP 100%
        	txtInversion100.setText(viewTop100.getTituloInversion());
        	if (listaTop100.size() >= 6) {
        		name100Top1.setText(viewTop100.getTop1().getNombre());
        		name100Top2.setText(viewTop100.getTop2().getNombre());
        		name100Top3.setText(viewTop100.getTop3().getNombre());
        	}
    	} else {
    		
    	}


		
		// MOVIMIENTO DE VENTANA DESDE TOP WINDOWS
		topWindow.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
		
		topWindow.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
            	fStage.setX(event.getScreenX() - xOffset);
            	fStage.setY(event.getScreenY() - yOffset);
            }
        });
		
		// CERRAR VENTANA MEDIANTE BOTON CLOSE
		btnClose.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

		     @Override
		     public void handle(MouseEvent event) {
		    	 fStage.close();		         
		         event.consume();
		     }
		});
		
		// MINIMIZAR VENTANA MEDIANTE BOTON MINIMIZE
		btnMinimize.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

		     @Override
		     public void handle(MouseEvent event) {
		    	 fStage.setIconified(true);	         
		         event.consume();
		     }
		});
		
		// RETROCEDER A MAINWINDOW MEDIANTE BOTON BACK
		btnBack.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

		     @Override
		     public void handle(MouseEvent event) {
		    	 tStage.setX(event.getScreenX() - xOffset);
		    	 tStage.setY(event.getScreenY() - yOffset);
		    	 tStage.show();
		         fStage.hide();		         
		         event.consume();
		     }
		});
		
	}

	
	
	
}
