package frontEnd;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

import javax.swing.ButtonModel;
import javax.swing.plaf.basic.BasicSplitPaneUI.KeyboardEndHandler;

import AnalyticAHP.Analityc;
import AnalyticAHP.Smartphone;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.transformation.SortedList;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import webScrap.JsoupRun;

public class investmentWindowController extends Application implements Initializable{

    @FXML private ImageView btnClose;
    @FXML private ImageView btnMinimize;
    @FXML private ImageView btnNext;
    @FXML private AnchorPane topWindow;
    @FXML private ImageView btnBack;
    @FXML private TextField textInvestment;
    @FXML private TextField textUsed;
    @FXML private TextField textTotal;
    @FXML private Label bottomLabel = new Label();
    @FXML private Label errorLabel;
    
    @FXML	private Stage pStage; 
    @FXML	private Stage sStage; 
    @FXML	private Stage tStage = new Stage(); 
    
    private boolean styled = false; // Permite verificar si ya fue removido el marco de la ventana. 

    private Analityc analityc = new Analityc();
	private Task getUsedPriceWorker;

	private boolean valorReventaEncontrdo = false; 
	private JsoupRun offlineFlag = new JsoupRun();
    
	private double xOffset = 0; 
	private double yOffset = 0;
	
	
	public investmentWindowController(Stage pStage, Stage sStage, Analityc analityc) {
		this.analityc = analityc;
		this.pStage = pStage; 
		this.sStage = sStage; 
	}
	
	// SIGUIENTE VENTANA priorityWindow mediante btnNext
	public void nextPriorityWindow() {
		try {	
			if (textInvestment.getText().matches("[0-9]*" + "." + "[0-9]*") && !textInvestment.getText().isEmpty() && textUsed.getText().matches("[0-9]*.[0-9]*") && !textUsed.getText().isEmpty()){
				xOffset = sStage.getX();
	            yOffset = sStage.getY();
	            analityc.setValorTotalInversion(Float.parseFloat(textTotal.getText()));
	            
	    			System.out.println("Numero de threads activos: " + analityc.getGrupoThread().activeCount());
		            this.start(tStage);
					sStage.hide();
					errorLabel.setVisible(false);
			} 
			else {
				errorLabel.setVisible(true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	// REALIZA LA SUMA DE INVERSION + VALOR DE REVENTA
	private synchronized void setSumaTotal() {
		double sumaTotal = 0; 
		if (textInvestment.getText().matches("[0-9]*" + "." + "[0-9]*") && !textInvestment.getText().isEmpty()) {
			sumaTotal += Double.parseDouble(textInvestment.getText());
		}
		if (textUsed.getText().matches("[0-9]*.[0-9]*") && !textUsed.getText().isEmpty()) {
			sumaTotal += Double.parseDouble(textUsed.getText());
		}
		textTotal.setText(sumaTotal + "");
	}
	
	public void backFromPriorityWindow() {
		bottomLabel.setVisible(false);
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		// BUSCO VALOR DE REVENTA DE SMARTPHONE SELECCIONADO
		
		valorReventaEncontrdo = false;
		getUsedPriceWorker = createUsedPriceWorker();
		new Thread(getUsedPriceWorker).start();		
		
		// SUMAR VALORES INVERSION + REVENTA
		
		textInvestment.setOnKeyReleased(e -> {
			setSumaTotal();
		});
		
		textUsed.setOnKeyReleased(e -> {
			setSumaTotal();
		});
		
		// COPIAR VALOR DE REVENTA SUGUERIDO EN TEXTUSED
		
		bottomLabel.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (valorReventaEncontrdo) {
					textUsed.setText(analityc.getDispositivoElegido().getPrecioUsado() + "");
					setSumaTotal();
				}
			}
		});
	
		// MOVIMIENTO DE VENTANA DESDE TOP WINDOWS
		topWindow.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				xOffset = event.getSceneX();
				yOffset = event.getSceneY();
			}
		});	

		topWindow.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				sStage.setX(event.getScreenX() - xOffset);
				sStage.setY(event.getScreenY() - yOffset);
			}
		});

		// CERRAR VENTANA MEDIANTE BOTON CLOSE
		btnClose.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				sStage.close();		         
				event.consume();
			}
		});

		// MINIMIZAR VENTANA MEDIANTE BOTON MINIMIZE
		btnMinimize.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				sStage.setIconified(true);	         
				event.consume();
			}
		});

		// RETROCEDER A MAINWINDOW MEDIANTE BOTON BACK
		btnBack.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				pStage.setX(event.getScreenX() - xOffset);
				pStage.setY(event.getScreenY() - yOffset);
				pStage.show();
				sStage.hide();	
				event.consume();
			}
		});
				
	}


	@Override
	public void start(Stage thirdStage) throws Exception {
		
		FXMLLoader loader = new FXMLLoader(getClass().getResource("priorityWindow.fxml"));
		priorityWindowController c = new priorityWindowController(sStage, thirdStage, analityc, this); 
		loader.setController(c);
		Pane mainPane = (Pane) loader.load();
		Scene scene = new Scene(mainPane);
		thirdStage.setScene(scene);
		thirdStage.setX(xOffset);
		thirdStage.setY(yOffset);
		if (!styled) {
			thirdStage.initStyle(StageStyle.UNDECORATED);
			styled = true;
		}
		thirdStage.setWidth(pStage.getWidth());
		thirdStage.setHeight(pStage.getHeight());
		thirdStage.show();
		mainPane.requestFocus();
		
		
	}
	

	
	
		
	public synchronized Task createUsedPriceWorker() {
    	return new Task() {

			@Override
			protected Object call() throws Exception {
				JsoupRun js = new JsoupRun();
				try {
					bottomLabel.setVisible(true);
					if (offlineFlag.hayConexion()) {
						valorReventaEncontrdo = js.obtenerPrecioUsado(analityc.getDispositivoElegido());
						if (valorReventaEncontrdo) {
							Platform.runLater( // "Platform.runLater" PERMITE OMITIR EL WARNING EN EJECUCION YA QUE SE ESTA EDITANDO EL TEXTO DESDE OTRO THREAD
									  () -> {
											bottomLabel.setText("Valor de reventa suguerido: $" + String.valueOf(analityc.getDispositivoElegido().getPrecioUsado()));
											bottomLabel.setStyle("-fx-background-color:  #00C853;");
									  }
									);

						}else {
							Platform.runLater( // "Platform.runLater" PERMITE OMITIR EL WARNING EN EJECUCION YA QUE SE ESTA EDITANDO EL TEXTO DESDE OTRO THREAD
									  () -> {
											bottomLabel.setText("Lamentablemente no encontramos un valor de reventa suguerido :(");
											bottomLabel.setStyle("-fx-background-color: #283747;");								  }
									);

						}
					} else {
						Platform.runLater( // "Platform.runLater" PERMITE OMITIR EL WARNING EN EJECUCION YA QUE SE ESTA EDITANDO EL TEXTO DESDE OTRO THREAD
								  () -> {
										bottomLabel.setText("Sin conexi�n.");
										bottomLabel.setStyle("-fx-background-color: #283747;");								  }
								);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(analityc.getDispositivoElegido().getPrecioUsado());
				return null;
			}
    	};
	}
}
