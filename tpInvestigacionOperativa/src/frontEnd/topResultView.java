package frontEnd;

import AnalyticAHP.Smartphone;
import javafx.scene.text.Text;

public class topResultView {

	private String tituloInversion;	
	private Smartphone top1; 
	private Smartphone top2; 
	private Smartphone top3;
	private float score1;
	private float score2;
	private float score3;
	
	
	public topResultView(Smartphone top1, Smartphone top2, Smartphone top3, String titulo) {
		this.top1 = top1; 
		this.top2 = top2; 
		this.top3 = top3; 
		tituloInversion = titulo;
	}
	
	public topResultView(Smartphone top1, float score1, Smartphone top2, float score2, Smartphone top3, float score3, String titulo) {
		this.top1 = top1; 
		this.top2 = top2; 
		this.top3 = top3;
		tituloInversion = titulo;
		this.score1 = score1;
		this.score2 = score2;
		this.score3 = score3;
	}
	
	public String getTituloInversion() {
		return tituloInversion;
	}

	public Smartphone getTop1() {
		return top1;
	}

	public Smartphone getTop2() {
		return top2;
	}

	public Smartphone getTop3() {
		return top3;
	}

	public float getScore1() {
		return score1;
	}

	public float getScore2() {
		return score2;
	}

	public float getScore3() {
		return score3;
	}
	
}
