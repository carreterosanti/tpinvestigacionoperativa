package frontEnd;

import AnalyticAHP.AHP_Smartphone;
import javafx.scene.image.Image;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

public class priorityScoreView {
	
	private Text txtVs;
	private Rectangle colorVs1;
	private Rectangle colorVs2;
	private Text nroVs1;
	private Text nroVs2;
	private priorityWindowController controller; 
	private String vs1; 
	private String vs2; 
	private Image img1; 
	private Image img2; 

	
	public priorityScoreView(Text txt, Rectangle color1, Rectangle color2, Text nro1, Text nro2, priorityWindowController controller, Image img1, Image img2, String vs1, String vs2) {
		txtVs = txt; 
		colorVs1 = color1;
		colorVs2 = color2; 
		nroVs1 = nro1; 
		nroVs2 = nro2; 		
		this.controller = controller; 
		this.img1 = img1;
		this.img2 = img2;
		this.vs1 = vs1;
		this.vs2 = vs2;
		
		
	}
	
	public void deleteScores() {
		txtVs.setFill(Paint.valueOf("#90a4ae"));
		colorVs1.setFill(Paint.valueOf("#546e7a"));
		colorVs1.setStroke(Paint.valueOf("#455a64"));
		colorVs2.setFill(Paint.valueOf("#546e7a"));
		colorVs2.setStroke(Paint.valueOf("#455a64"));
		nroVs1.setText("X");
		nroVs2.setText("X");
		nroVs1.setFill(Paint.valueOf("#b0bec5"));
		nroVs2.setFill(Paint.valueOf("#b0bec5"));
	}
	
	public int escalaAHP(int valorUsuario) { // RETORNA EL VALOR DE PRIORIDAD DEL CRITERIO  DE (=-1-2-3-4-5) A (1-3-4-5-7-9)  
		switch (valorUsuario) {
		case 0:
			return 1;
		case 1:
			return 3;
		case 2:
			return 4;
		case 3:
			return 5;
		case 4:
			return 7;
		case 5:
			return 9;
		}
		return -1;
	}
	
	public void selectScore(int score) {
		controller.setNextVs();
		
			if (score < 0) {
				deleteScores();
				txtVs.setFill(Paint.valueOf("#FFFFFF"));
				colorVs1.setFill(Paint.valueOf("#1abc9c"));
				colorVs1.setStroke(Paint.valueOf("#1abc9c"));
				nroVs1.setText(-score + "");
				nroVs1.setFill(Paint.valueOf("#FFFFFF"));
				controller.getAnalityc().getAhp_Smartphone().setValorMatrizCriterios(AHP_Smartphone.getConstanteCriterio(vs1), AHP_Smartphone.getConstanteCriterio(vs2), escalaAHP((-score)));
			} else if (score > 0){
				deleteScores();
				txtVs.setFill(Paint.valueOf("#FFFFFF"));
				colorVs2.setFill(Paint.valueOf("#1abc9c"));
				colorVs2.setStroke(Paint.valueOf("#1abc9c"));
				nroVs2.setText(score + "");
				nroVs2.setFill(Paint.valueOf("#FFFFFF"));
				controller.getAnalityc().getAhp_Smartphone().setValorMatrizCriterios(AHP_Smartphone.getConstanteCriterio(vs2), AHP_Smartphone.getConstanteCriterio(vs1), escalaAHP((score)));
			} else {
				colorVs1.setFill(Paint.valueOf("#1abc9c"));
				txtVs.setFill(Paint.valueOf("#FFFFFF"));
				colorVs1.setStroke(Paint.valueOf("#1abc9c"));
				colorVs2.setFill(Paint.valueOf("#1abc9c"));
				colorVs2.setStroke(Paint.valueOf("#1abc9c"));
				nroVs1.setText("X");
				nroVs2.setText("X");
				controller.getAnalityc().getAhp_Smartphone().setValorMatrizCriterios(AHP_Smartphone.getConstanteCriterio(vs1), AHP_Smartphone.getConstanteCriterio(vs2), escalaAHP((score)));
			}
			
	}
	
	
	public Text getTxtVs() {
		return txtVs;
	}

	public Rectangle getColorVs1() {
		return colorVs1;
	}

	public Rectangle getColorVs2() {
		return colorVs2;
	}

	public Text getNroVs1() {
		return nroVs1;
	}

	public Text getNroVs2() {
		return nroVs2;
	}

	public priorityWindowController getController() {
		return controller;
	}

	public String getVs1() {
		return vs1;
	}

	public String getVs2() {
		return vs2;
	}

	public Image getImg1() {
		return img1;
	}

	public Image getImg2() {
		return img2;
	}


	
	
	
	
}
