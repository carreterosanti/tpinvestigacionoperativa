package admArchivos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class admArchivo {
	
	
	public BufferedReader getListaCelulares() throws FileNotFoundException {
		File file = new File("C:\\Users\\joaco\\git\\tpinvestigacionoperativa\\tpInvestigacionOperativa\\src\\admArchivos\\ListaCelularesGamaAlta.txt");
		
		BufferedReader br = new BufferedReader(new FileReader(file));
		
		return br; 
	}
	
	public void generarBaseDeDatos(List<String> listaSmartphones) throws IOException {
		Path file = Paths.get("Lista Celulares todas las Gamas.txt");
		Files.write(file, listaSmartphones, Charset.forName("UTF-8"));
	}
	
	public BufferedReader levantarBaseDeDatos(String rutaArchivo) {
		
		
		try {
			File file = new File(rutaArchivo);
			BufferedReader br = new BufferedReader(new FileReader(file));
			return br;			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
		

	}
	
	
	
 
	
	//TESTEO LECTOR 
	public static void main(String[] args) throws IOException {
		
		admArchivo lA = new admArchivo(); 
		lA.levantarBaseDeDatos("Lista Celulares todas las Gamas.txt");
		
	}
}
